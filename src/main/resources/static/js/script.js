
function getNote(){

    var url  = "http://localhost:8080/note";
    var xhr  = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function () {
        console.log(xhr.responseText);

        var note = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {

            var htmlElemTitle = document.getElementById('noteTitle');
            htmlElemTitle.innerHTML = note.title;

            document.getElementById('myNote').removeAttribute('hidden');

        } else {
           alert('erro no pedido');
        }
    }
    xhr.send(null);
}

function checkLogin(e){
    e.preventDefault();

    var data = {};
    data.username = document.getElementById("app_username").value;
    data.password  = document.getElementById("app_Password").value;

    var json = JSON.stringify(data);
    var url  = "http://localhost:8080/login";
    var xhr  = new XMLHttpRequest();
    xhr.open('POST', url, true)
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.onload = function () {
        console.log(xhr.responseText);

        var token = JSON.parse(xhr.responseText).token;
        if (xhr.readyState == 4 && xhr.status == "200") {
            // save token in browser's local storage
            window.localStorage.setItem('token',token);
            // change to user view
            changeToUserHomePage();
        } else {
            alert('erro no pedido');
        }
    }
    xhr.send(json);
}

function changeToUserHomePage(){
    document.getElementById("appLogin").hidden = true;
    document.getElementById("appAddNote").hidden =  true;
    document.getElementById("appNoteList").hidden = false;
    getNotes();
}


function getNotes(){
    var url  = "http://localhost:8080/notes";
    var xhr  = new XMLHttpRequest();
    xhr.open('GET', url, true);

    // get autorization token from local storage
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem('token'));
    xhr.onload = function () {
        console.log(xhr.responseText);

        var notes = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            document.getElementById('app_notes').innerHTML = "";
            if (notes == null || notes.length==0){
                document.getElementById('app_notes').innerHTML = "No notes.."
            }
            for(var i =0; i< notes.length; i++){

                document.getElementById('app_notes').innerHTML += "<li class='list-group-item'>Note: "+notes[i].title+" Description: "+ notes[i].description+"</li>";
            }
        } else {
            alert('error in request');
        }
    }
    xhr.send(null);
}

function postNote(e){
    e.preventDefault();

    var url = "http://localhost:8080/note";

    var data = {};
    data.title = document.getElementById("app_note_title").value;
    data.description  = document.getElementById("app_note_description").value;

    var json = JSON.stringify(data);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.onload = function () {
        var note = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            changeToUserHomePage();
        } else {
            // Error
        }
    }
    xhr.send(json);
}

function showAddNote(){
    document.getElementById("appLogin").hidden = true;
    document.getElementById("appAddNote").hidden =  false;
    document.getElementById("appNoteList").hidden = true;
    getNotes();
}
